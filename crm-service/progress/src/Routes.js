import React from 'react'
import { Switch, Route } from 'react-router-dom'

import AuthPage from './Pages/Auth'
import UsersPage from './Pages/Users'
import NotFoundPage from './Pages/NotFound'
import LogoutPage from './Pages/Logout'
import CategoriesPage from './Pages/Categories'
import CategoryPage from './Pages/Category';
import ProductsPage from './Pages/Products'
import OrdersPage from './Pages/Orders'

class Routes extends React.Component {
	render() {
		return (
			<Switch>
				<Route exact path="/auth">
					<AuthPage />
				</Route>
				<Route exact path="/users">
					<UsersPage />
				</Route>
				<Route exact path="/categories">
					<CategoriesPage />
				</Route>
				<Route exact path="/categories/:id" component={CategoryPage} />
				<Route exact path="/products">
					<ProductsPage />
				</Route>
				<Route exact path="/orders">
					<OrdersPage />
				</Route>
				<Route exact path="/logout">
					<LogoutPage />
				</Route>
				<Route path="*">
					<NotFoundPage />
				</Route>
			</Switch>
		)
	}
}

export default Routes;
