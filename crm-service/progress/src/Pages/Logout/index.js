import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { logout } from '../../Storage/Actions/App'

import { Logout } from '../../Services/Users'


class LogoutPage extends React.Component {
	render(){
		if(this.props.app.isAuth) {
			Logout();
			this.props.logout();
		}

		return <Redirect to="/auth" />
	}
}

export default connect(
	({ app }) => ({ app }),
	(dispatch) => ({
		logout: logout(dispatch)
	})
)(LogoutPage)