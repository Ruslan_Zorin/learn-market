import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Link } from 'react-router-dom'
import { Row, Button, Modal, Form, Input, Table, Col } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import {
	switchModal,
	clearNewCategory,
	changeCategoryName,
	changeCategoryCode,
	changeCategoryDescription,
	setCategories,
	changeFiltersName,
	clearFilters,
	changeFiltersCode,
} from '../../Storage/Actions/Pages/Categories'
import { setTitle } from '../../Storage/Actions/App'

import { List, Create } from '../../Services/Categories'
import './Categories.css'


class CategoriesPage extends React.Component {
	constructor() {
		super()

		this.openModel = this.openModel.bind(this)
		this.onClickCreate = this.onClickCreate.bind(this)
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangeCode = this.onChangeCode.bind(this);
		this.onChangeDescription = this.onChangeDescription.bind(this);
		this.onChangeFiltersName = this.onChangeFiltersName.bind(this);
		this.onChangeFiltersCode = this.onChangeFiltersCode.bind(this);
		this.onClickSearchButton = this.onClickSearchButton.bind(this);
		this.onClickResetFilters = this.onClickResetFilters.bind(this);
	}

	async componentDidMount() {
		this.props.setTitle('Управление категориями');
		const { nav, filters } = this.props.page
		const { categories } = await List(filters, nav)

		this.props.setCategories(categories)
	}

	render() {
		if (!this.props.app.isAuth)
			return <Redirect to="/auth" />

		return (
			<div>
				<Row>
					<Button type="primary" onClick={this.openModel} className="create-category-button">Добавить категорию</Button>
				</Row>
				<Form layout="vertical">
					<Row justify="space-between" align="middle">
						<Col span={9}>
							<Form.Item label="Название категории">
								<Input placeholder="Введите название категории" value={this.props.page.filters.name} onChange={this.onChangeFiltersName}  />
							</Form.Item>
						</Col>
						<Col span={9}>
							<Form.Item label="Код категории">
								<Input placeholder="Введите код категории" value={this.props.page.filters.code} onChange={this.onChangeFiltersCode} />
							</Form.Item>
						</Col>
						<Col span={2}>
							<Button type="primary" icon={<SearchOutlined />} className="search-button" onClick={this.onClickSearchButton}>
								Поиск
							</Button>
						</Col>
						<Col span={2}>
							<Button className="search-button" onClick={this.onClickResetFilters}>
								Сбросить
							</Button>
						</Col>
					</Row>
				</Form>
				<Table
					columns={this.getColumns()}
					dataSource={this.props.page.list}
				/>
				<Modal
					title="Добавление категории"
					visible={this.props.page.isShowModel}
					footer={[
						<Button key="back" onClick={this.props.switchModal}>
							Отмена
						</Button>,
						<Button key="submit" type="primary" onClick={this.onClickCreate}>
							Создать
						</Button>,
					]}
				>
					<Form
						layout="vertical"
					>
						<Form.Item
							label="Название категории"
							name="name"
							rules={[{ required: true, message: 'Пожалуйста, введите название категории!' }]}
						>
							<Input value={this.props.page.newCategory.name} onChange={this.onChangeName} />
						</Form.Item>
						<Form.Item
							label="Код категории"
							name="code"
							rules={[{ required: true, message: 'Пожалуйста, введите код категории!' }]}
						>
							<Input value={this.props.page.newCategory.code} onChange={this.onChangeCode} />
						</Form.Item>
						<Form.Item
							label="Описание категории"
							name="description"
						>
							<Input.TextArea value={this.props.page.newCategory.description} onChange={this.onChangeDescription} rows={8} />
						</Form.Item>
					</Form>

				</Modal>
			</div>
		)
	}

	openModel() {
		this.props.switchModal();
	}

	async onClickSearchButton() {
		const { nav, filters } = this.props.page

		const { categories } = await List(filters, nav)

		this.props.setCategories(categories)
	}

	async onClickCreate() {
		await Create(this.props.page.newCategory)
		const { categories } = await List()

		this.props.setCategories(categories)
		this.props.switchModal();
		this.props.clearNewCategory();
	}

	async onClickResetFilters() {
		this.props.clearFilters();
		const { nav } = this.props.page
		const { categories } = await List({}, nav)

		this.props.setCategories(categories)
	}

	onChangeName(event) {
		const { value: name } = event.target;
		this.props.changeCategoryName(name);
	}

	onChangeCode(event) {
		const { value: code } = event.target;
		this.props.changeCategoryCode(code);
	}

	onChangeDescription(event) {
		const { value: description } = event.target;
		this.props.changeCategoryDescription(description);
	}

	onChangeFiltersName(event) {
		const { value: name } = event.target;
		this.props.changeFiltersName(name);
	}

	onChangeFiltersCode(event) {
		const { value: code } = event.target;

		this.props.changeFiltersCode(code);
	}

	getColumns() {
		return [
			{
				title: 'Название категории',
				dataIndex: 'name',
				key: 'name',
			},
			{
				title: 'Код категории',
				dataIndex: 'code',
				key: 'code',
			},
			{
				title: 'Описание категории',
				dataIndex: 'description',
				key: 'description',
			},
			{
				title: 'Действия',
				dataIndex: 'id',
				key: 'id',
				render: (id) => {
					return (
						<Link to={`/categories/${id}`}>Подробно</Link>
					);
				}
			}
		]
	}
}


export default connect(
	({ app, categoriesPage }) => ({ page: categoriesPage, app }),
	(dispatch) => ({
		switchModal: switchModal(dispatch),
		setTitle: setTitle(dispatch),
		changeCategoryName: changeCategoryName(dispatch),
		changeCategoryCode: changeCategoryCode(dispatch),
		changeCategoryDescription: changeCategoryDescription(dispatch),
		clearNewCategory: clearNewCategory(dispatch),
		setCategories: setCategories(dispatch),
		changeFiltersName: changeFiltersName(dispatch),
		changeFiltersCode: changeFiltersCode(dispatch),
		clearFilters: clearFilters(dispatch),
	})
)(CategoriesPage)
