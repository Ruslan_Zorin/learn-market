import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { format } from 'date-fns'
import { ru } from 'date-fns/locale'
import { SearchOutlined } from '@ant-design/icons';

import {
	Table,
	Tag,
	Row,
	Col,
	Form,
	Input,
	Select,
	Button,
} from 'antd';

import { List, AddRole } from '../../Services/Users'

import { setUsers, changePhone, changeRole, changeSex, changePage } from '../../Storage/Actions/Pages/Users'
import { setTitle } from '../../Storage/Actions/App'
import { Roles, Sex } from '../../Constants'

import './Users.css'


class UsersPage extends React.Component {
	constructor() {
		super();

		this.onChangePhone = this.onChangePhone.bind(this)
		this.onChangeRole = this.onChangeRole.bind(this)
		this.onChangeSex = this.onChangeSex.bind(this)
		this.onClickSearchButton = this.onClickSearchButton.bind(this)
		this.onChangePage = this.onChangePage.bind(this)
		this.onClickAddRole = this.onClickAddRole.bind(this)
	}

	async componentDidMount() {
		this.props.setTitle('Управление пользователями')
		const { nav } = this.props.page;
		const usersList = await List(nav);
		this.props.setUsers(usersList)
	}


	render() {
		if (!this.props.app.isAuth)
			return <Redirect to="/auth" />

		const usersData = this.props.page.users.map((user) => ({
			key: user.id,
			name: `${user.name} ${user.surname}`,
			birthday: user.birthday || '',
			phone: user.phone,
			sex: user.sex || 'unknown',
			role: user.role,
			date_registration: user.date_registration,
		}));

		return (
			<div>
				<Form layout="vertical">
					<Row justify="space-between" align="middle">
						<Col span={7}>
							<Form.Item label="Номер телефона">
								<Input placeholder="Введите телефон" value={this.props.page.filters.phone} onChange={this.onChangePhone} />
							</Form.Item>
						</Col>
						<Col span={7}>
							<Form.Item label="Роль">
								<Select onChange={this.onChangeRole} placeholder="Выберите роль">
									{
										Object.keys(Roles).map((role) =>(
											<Select.Option key={role} value={role}>{Roles[role]}</Select.Option>
										))
									}
								</Select>
							</Form.Item>
						</Col>
						<Col span={7}>
							<Form.Item label="Пол">
								<Select onChange={this.onChangeSex} placeholder="Выберите пол">
									{
										Object.keys(Sex).map((item) =>(
											<Select.Option key={item} value={item}>{Sex[item]}</Select.Option>
										))
									}
								</Select>
							</Form.Item>
						</Col>
						<Col span={2}>
							<Button type="primary" icon={<SearchOutlined />} className="search-button" onClick={this.onClickSearchButton}>
								Поиск
							</Button>
						</Col>
					</Row>
				</Form>
				<Table
					columns={this.getColumns()}
					dataSource={usersData}
					pagination={{
						total: this.props.page.total,
						current: this.props.page.nav.page,
						pageSize: this.props.page.nav.lpage,
						onChange: this.onChangePage,
					}}
				/>
			</div>
		);
	}

	onChangePhone(event) {
		const { value: phone } = event.target;
		this.props.changePhone(phone)
	}

	onChangeRole(role) {
		this.props.changeRole(role)
	}

	onChangeSex(sex) {
		this.props.changeSex(sex)
	}

	async onClickSearchButton() {
		const { filters, nav } = this.props.page;
		
		const preparedFilters = Object.keys(filters).reduce((acc, filter) => {
			if(!filters[filter])
				return acc;

			acc[filter] = filters[filter];
			return acc;
		}, nav)

		const usersList = await List(preparedFilters);
		this.props.setUsers(usersList)
	}

	async onChangePage(page, pageSize) {
		this.props.changePage(page)
		const { filters, nav } = this.props.page;
		
		const preparedFilters = Object.keys(filters).reduce((acc, filter) => {
			if(!filters[filter])
				return acc;

			acc[filter] = filters[filter];
			return acc;
		}, nav)

		const usersList = await List(preparedFilters);
		this.props.setUsers(usersList)
	}

	async onClickAddRole(userId) {
		await AddRole(userId);
		const { filters, nav } = this.props.page;
		
		const preparedFilters = Object.keys(filters).reduce((acc, filter) => {
			if(!filters[filter])
				return acc;

			acc[filter] = filters[filter];
			return acc;
		}, nav)

		const usersList = await List(preparedFilters);
		this.props.setUsers(usersList)
	}

	getColumns() {
		return [
			{
				title: 'Имя Фамилия',
				dataIndex: 'name',
				key: 'name',
			},
			{
				title: 'Пол',
				dataIndex: 'sex',
				key: 'sex',
				render: (sex) => {
					return Sex[sex]
				}
			},
			{
				title: 'День рождения',
				dataIndex: 'birthday',
				key: 'birthday',
				render: (birthday) => {
					if (!birthday)
						return 'Нет'

					return format(new Date(birthday), 'dd LLLL yyyy', { locale: ru })
				}
			},
			{
				title: 'Телефон',
				dataIndex: 'phone',
				key: 'phone',
			},
			{
				title: 'Роль',
				dataIndex: 'role',
				key: 'role',
				render: (role) => {
					return <Tag color={role === 'user'
						? 'green'
						: 'red'
					}>{Roles[role]}</Tag>
				}
			},
			{
				title: 'Дата Регистрации',
				dataIndex: 'date_registration',
				key: 'date_registration',
				render: (date_registration) => {
					return format(new Date(date_registration), 'dd LLLL yyyy', { locale: ru })
				}
			},
			{
				title: 'Управление',
				dataIndex: 'control',
				key: 'control',
				render: (text, record) => {
					return (
						<Button
							onClick={() => this.onClickAddRole(record.key)}
							type="dashed"
						>
							<span>
								{
									record.role === 'admin'
										? 'Забрать права'
										: 'Добавить права'
								}
							</span>
						</Button>
					)
				}
			},
		];
	}
}



export default connect(
	({ app, usersPage }) => ({ page: usersPage, app }),
	(dispatch) => ({
		setUsers: setUsers(dispatch),
		changePhone: changePhone(dispatch),
		changeRole: changeRole(dispatch),
		changeSex: changeSex(dispatch),
		changePage: changePage(dispatch),
		setTitle: setTitle(dispatch),
	})
)(UsersPage)