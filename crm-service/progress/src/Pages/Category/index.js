import React from 'react'
import { Row, Button, Modal, Form, Input, Table, Col } from 'antd';
import { connect } from 'react-redux'
import { GetById, Update } from '../../Services/Categories'
import {
	switchModal,
	setCategory,
	changeName,
	changeCode,
	changeDescription,
} from '../../Storage/Actions/Pages/Category'
import { setTitle } from '../../Storage/Actions/App'

import './Category.css'


class CategoryPage extends React.Component {
	constructor() {
		super();

		this.onChangeName = this.onChangeName.bind(this);
		this.onChangeCode = this.onChangeCode.bind(this);
		this.onChangeDescription = this.onChangeDescription.bind(this);
		this.onClickSave = this.onClickSave.bind(this)
	}
	async componentDidMount() {
		const { category } = await GetById(this.props.match.params.id);

		this.props.setTitle(`Страница категории - ${category.name}`)
		this.props.setCategory(category)
	}

	render() {
		// console.log(this.props.page)
		return (
			<div>
				<Row>
					<Button type="primary" className="show-edit-button" onClick={this.props.switchModal}>Изменить</Button>
				</Row>
				<p><b>Название категории</b>: {this.props.page.name}</p>
				<p><b>Код категории</b>: {this.props.page.code}</p>
				<p><b>Описание категории</b>: {this.props.page.description}</p>
				<Modal
					title="Изменение категории"
					visible={this.props.page.isShowModal}
					footer={[
						<Button key="back" onClick={this.props.switchModal}>
							Отмена
						</Button>,
						<Button key="submit" type="primary" onClick={this.onClickSave}>
							Сохранить
						</Button>,
					]}
				>
					<Form
						layout="vertical"
					>
						<Form.Item
							label="Название категории"
							name="name"
							rules={[{ required: true, message: 'Пожалуйста, введите название категории!' }]}
							initialValue={this.props.page.updatedData.name}
						>
							<Input value={this.props.page.updatedData.name} onChange={this.onChangeName} />
						</Form.Item>
						<Form.Item
							label="Описание категории"
							name="description"
							initialValue={this.props.page.updatedData.description}
						>
							<Input.TextArea
								value={this.props.page.updatedData.description}
								onChange={this.onChangeDescription}
								rows={8}
							/>
						</Form.Item>
					</Form>
				</Modal>
			</div>
		)
	}

	onChangeName(event) {
		const { value: name } = event.target;
		this.props.changeName(name);
	}

	onChangeCode(event) {
		const { value: code } = event.target;
		this.props.changeCode(code);
	}

	onChangeDescription(event) {
		const { value: description } = event.target;
	
		this.props.changeDescription(description);
	}

	async onClickSave() {
		await Update(this.props.match.params.id, {
			name: this.props.page.updatedData.name,
			description: this.props.page.updatedData.description,
		});
		const { category } = await GetById(this.props.match.params.id);

		this.props.setTitle(`Страница категории - ${category.name}`)
		this.props.setCategory(category)
		this.props.switchModal();
	}
}

export default connect(
	({ app, categoryPage }) => ({ page: categoryPage, app }),
	(dispatch) => ({
		setTitle: setTitle(dispatch),
		switchModal: switchModal(dispatch),
		setCategory: setCategory(dispatch),
		changeName: changeName(dispatch),
		changeCode: changeCode(dispatch),
		changeDescription: changeDescription(dispatch),
	})
)(CategoryPage)