import React from 'react'
import { connect } from 'react-redux'

import { setTitle } from '../../Storage/Actions/App'

import './NotFound.css'

class NotFoundPage extends React.Component {
	componentDidMount() {
		this.props.setTitle('Страница не найдена')
	}

	render() {
		return (
			<h1>Ошибка 404</h1>
		);
	}

}

export default connect(
	() => ({}),
	(dispatch) => ({
		setTitle: setTitle(dispatch),
	})
)(NotFoundPage);