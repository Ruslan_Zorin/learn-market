import React from 'react'
import { Form, Input, Row, Col } from 'antd';
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import { GetCode, GetToken } from '../../Services/Users'
import { changePhone, changeCode } from '../../Storage/Actions/Pages/Auth'
import { updateAuth, setTitle } from '../../Storage/Actions/App'

import './Auth.css'

class AuthPage extends React.Component {
	constructor() {
		super();

		this.onChangePhone = this.onChangePhone.bind(this)
		this.onChangeCode = this.onChangeCode.bind(this)
	}

	componentDidMount() {
		this.props.setTitle('Авторизация/Вход')
	}

	render() {
		if(this.props.app.isAuth)
			return <Redirect to="/users" />

		return (
			<Form layout="vertical" >
				<Row>
					<Col span={6}>
						<Form.Item label="Телефон">
							<Input
								placeholder="Телефон"
								onChange={this.onChangePhone}
								value={this.props.page.phone}
							/>
						</Form.Item>
						<Form.Item label="Код" hidden={this.props.page.isHiddenCode}>
							<Input
								placeholder="Код"
								onChange={this.onChangeCode}
								value={this.props.page.code}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Form>
		);
	}

	async onChangePhone(event) {
		const { value: phone } = event.target;
		this.props.changePhone(phone)
		if(phone.length === 10) {
			await GetCode(phone)
		}
	}

	async onChangeCode(event) {
		const { value: code } = event.target;
		const { phone } = this.props.page
		this.props.changeCode(code)

		if(code.length === 6) {
			await GetToken(phone, code)
			this.props.updateAuth()
		}
	}
}



export default connect(
	({ authPage, app }) => ({ page: authPage, app }),
	(dispatch) => ({
		changePhone: changePhone(dispatch),
		changeCode: changeCode(dispatch),
		updateAuth: updateAuth(dispatch),
		setTitle: setTitle(dispatch),
	})
)(AuthPage)