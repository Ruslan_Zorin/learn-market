import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Table, notification } from 'antd';
import { List as OrdersList } from '../../Services/Orders'
import { setTitle } from '../../Storage/Actions/App'
import { setOrders, addOrder } from '../../Storage/Actions/Pages/Orders'
import Config from '../../Config'



class OrdersPage extends React.Component {
	constructor() {
		super();

		const ws = new WebSocket(Config.services.OrderSockets)

		ws.onopen = () => {
			notification.success({
				message: 'Подключение к серверу заказов успешно'
			});

			ws.send(JSON.stringify({
				type: 'auth',
				data: {
					token: localStorage.getItem('app-token')
				}
			}))
		}

		ws.onclose = () => {
			notification.error({
				message: 'Подключение к серверу заказов оборвано'
			})
		}

		ws.onmessage = (resp) => {
			const { data: order } = JSON.parse(resp.data);
			this.props.addOrder(order)
			notification.warn({
				message: 'Получен новый заказа'
			});
		}
	}

	async componentDidMount() {
		this.props.setTitle('Заказы')
		const { orders } = await OrdersList({ lpage: 20, page: 1 })
		this.props.setOrders(orders)
	}

	render() {
		console.log(this.props.page)
		return (
			<div>
				<Table 
					columns={this.getColumns()}
					dataSource={this.props.page.list}
				/>
			</div>
		)
	}

	getColumns() {
		return [
			{
				title: 'Телефон клиента',
				dataIndex: 'user',
				key: 'user',
				render: (user) => {
					return (
						<span>
							{ user && user.phone ? user.phone : 'НЕТ' }
						</span>
					)
				}
			},
			{
				title: 'Цена заказа',
				dataIndex: 'price',
				key: 'price',
			},
			{
				title: 'Количество продуктов',
				dataIndex: 'products',
				key: 'products',
				render: (products) => {
					return (
					<span>{products.length}</span>
					)
				}
			},
			{
				title: 'Комментарий к заказу',
				dataIndex: 'comments',
				key: 'comments',
			},
			{
				title: 'Комментарий к заказу',
				dataIndex: 'comments',
				key: 'comments',
			},
			{
				title: 'Действия',
				dataIndex: '_id',
				key: '_id',
				render: (id) => {
					return (
						<Link to={`/orders/${id}`}>Подробно</Link>
					);
				}
			},
		];
	}
}


export default connect(
	({ app, ordersPage }) => ({ page: ordersPage, app }),
	(dispatch) => ({
		setTitle: setTitle(dispatch),
		setOrders: setOrders(dispatch),
		addOrder: addOrder(dispatch),
	})
)(OrdersPage)

