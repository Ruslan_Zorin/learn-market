import React from 'react'
import Request from 'axios'
import { Row, Button, Modal, Form, Input, Table, Col, InputNumber, Select, Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { connect } from 'react-redux'
import { setTitle } from '../../Storage/Actions/App'
import {
	switchModal,
	changeName,
	changeVendorCode,
	changePrice,
	changeCategories,
	changeDescription,
	setCategories,
	changeId,
	addImages,
} from '../../Storage/Actions/Pages/Products'

import { List as GetCategories } from '../../Services/Categories'
import { Create } from '../../Services/Products'



class ProductsPage extends React.Component {
	constructor() {
		super();

		this.onChangeName = this.onChangeName.bind(this)
		this.onChangeVendorCode = this.onChangeVendorCode.bind(this)
		this.onChangePrice = this.onChangePrice.bind(this)
		this.onChangeCategories = this.onChangeCategories.bind(this)
		this.onChangeDescription = this.onChangeDescription.bind(this)
		this.onClickSave = this.onClickSave.bind(this);
		this.uploadImages = this.uploadImages.bind(this);
	}


	async componentDidMount() {
		this.props.setTitle('Продукты')
		const { categories } = await GetCategories({}, {})

		this.props.setCategories(categories)
	}

	render() {
		console.log(this.props.page.product)
		return (
			<div>
				<Row>
					<Button type="primary" className="show-edit-button" onClick={this.props.switchModal}>
						Добавить товар
					</Button>
				</Row>
				<Modal
					title="Добавить товар"
					visible={this.props.page.isShowModal}
					footer={[
						<Button key="back" onClick={this.props.switchModal}>
							Отмена
						</Button>,
						<Button key="submit" type="primary" onClick={this.onClickSave}>
							Сохранить
						</Button>,
					]}
				>
					<Form
						layout="vertical"
					>
						<Form.Item
							label="Название продукта"
							name="name"
							rules={[{ required: true, message: 'Введите название продукта' }]}
						>
							<Input onChange={this.onChangeName} />
						</Form.Item>
						<Form.Item
							label="Артикул проукта"
							name="vendor_code"
							rules={[{ required: true, message: 'Введите артикул продукта' }]}
						>
							<Input onChange={this.onChangeVendorCode} />
						</Form.Item>
						<Form.Item
							label="Стоимость продукта"
							name="price"
							rules={[{ required: true, message: 'Введите стоимость продукта' }]}
						>
							<InputNumber onChange={this.onChangePrice} min={0} />
						</Form.Item>
						<Form.Item
							label="Категории"
							name="categories"
							rules={[{ required: true, message: 'Выберите категории' }]}
						>
							<Select
								mode="multiple"
								style={{ width: '100%' }}
								placeholder="Выберите категории"
								onChange={this.onChangeCategories}
							>
								{
									this.props.page.categories.map((category) => (
										<Select.Option value={category.code} key={category.id}>
											{category.name}
										</Select.Option>
									))
								}
							</Select>
						</Form.Item>
						<Form.Item
							label="Описание продукта"
							name="description"
						>
							<Input.TextArea
								onChange={this.onChangeDescription}
								rows={8}
							/>
						</Form.Item>
						<Form.Item
							label="Описание продукта"
							name="images"

						>
							<Upload.Dragger
								disabled={false}
								multiple={true}
								customRequest={this.uploadImages}
							>
								<p className="ant-upload-drag-icon">
									<InboxOutlined />
								</p>
								<p className="ant-upload-text">Click or drag file to this area to upload</p>
								<p className="ant-upload-hint">
									Support for a single or bulk upload. Strictly prohibit from uploading company data or other
									band files
								</p>
							</Upload.Dragger>,
						</Form.Item>
					</Form>
				</Modal>
			</div>
		)
	}

	uploadImages(image) {
		this.props.addImages(image.file)
		image.onSuccess('OK')
		const url = `http://localhost:4000/products/5fcb6c1f8b4bde59b2a753e1/images`
		const formData = new FormData();
		formData.append('images', image.file)
		formData.append('name', this.props.page.product.name)
		formData.append('vendor_code', this.props.page.product.vendor_code)
		formData.append('price', this.props.page.product.price)
		formData.append('description', this.props.page.description)
		// await Request.post(url, formData, {
		// 	headers: {
		// 		'app-token': localStorage.getItem('app-token'),
		// 		'Content-Type': 'multipart/form-data'
		// 	}
		// });

	}

	async onClickSave() {
		const { product } = this.props.page;

		const data = Object.keys(product).reduce((acc, field) => {
			if(field === 'id')
				return acc;
			
			acc.append(field, product[field])
			return acc;
		}, new FormData())

		const { product: newProduct } = await Create(data)
	}

	onChangeName(event) {
		const { value: name } = event.target;
		this.props.changeName(name);
	}

	onChangeVendorCode(event) {
		const { value: vendorCode } = event.target;
		this.props.changeVendorCode(vendorCode);
	}

	onChangePrice(price) {
		this.props.changePrice(price);
	}

	onChangeCategories(categories) {
		this.props.changeCategories(categories);
	}

	onChangeDescription(event) {
		const { value: description } = event.target;
		this.props.changeDescription(description);
	}
}

export default connect(
	({ app, productsPage }) => ({ page: productsPage, app }),
	(dispatch) => ({
		setTitle: setTitle(dispatch),
		switchModal: switchModal(dispatch),
		changeName: changeName(dispatch),
		changeVendorCode: changeVendorCode(dispatch),
		changePrice: changePrice(dispatch),
		changeCategories: changeCategories(dispatch),
		changeDescription: changeDescription(dispatch),
		setCategories: setCategories(dispatch),
		changeId: changeId(dispatch),
		addImages: addImages(dispatch),
	})
)(ProductsPage)