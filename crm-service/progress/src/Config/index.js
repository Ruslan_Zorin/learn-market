const Config = {
	services: {
		Users: 'http://localhost:8000',
		Products: 'http://localhost:4000',
		Orders: 'http://localhost:3333',
		OrderSockets: 'ws://localhost:9000'
	},
};

export default Config
