import React from 'react'
import { Layout } from 'antd';
import { connect } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import Routes from './Routes'

import Sidebar from './Components/Sidebar'
import Header from './Components/Header'
import Footer from './Components/Footer'

import 'antd/dist/antd.css'
import './App.css';

const { Content } = Layout;

class App extends React.Component {
	
	render() {
		return (
			<Router>
				<div className="App">
					<Layout>
						<Sidebar isAuth={this.props.app.isAuth} />
						<Layout>
							<Header text={this.props.app.title} />
							<Content style={{ margin: '24px 16px 0' }}>
								<div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
									<Routes />
								</div>
							</Content>
							<Footer />
						</Layout>
					</Layout>
				</div>
			</Router>
		);
	}
}

export default connect(
	({ app }) => ({ app }),
	(dispatch) => ({})
)(App)

