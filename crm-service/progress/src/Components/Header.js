import React from 'react'
import { Layout } from 'antd';

const { Header: AntHeader } = Layout;

class Header extends React.Component {
	render() {
		return (
			<AntHeader className="site-layout-sub-header-background">
				<h2>{this.props.text}</h2>
			</AntHeader>
		);
	}
}

export default Header;
