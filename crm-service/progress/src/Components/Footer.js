import React from 'react'
import { Layout } from 'antd';


const { Footer: AntFooter } = Layout;

class Footer extends React.Component {
	render() {
		return (
			<AntFooter>НГУ ©2020 Created by НГУ team</AntFooter>
		);
	}
}

export default Footer;
