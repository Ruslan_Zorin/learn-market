import React from 'react'
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';

import { UploadOutlined, UserOutlined, VideoCameraOutlined, LoginOutlined, LogoutOutlined } from '@ant-design/icons';
const { Sider } = Layout;

class Sidebar extends React.Component {
	render() {
		return (
			<Sider>
				<div className="logo">
					<h1>My Market</h1>
				</div>
				<Menu theme="dark" mode="inline">
					{
						!this.props.isAuth && <Menu.Item key="6" icon={<LoginOutlined />}>
							<Link to="/auth">
								Вход
							</Link>
						</Menu.Item>
					}
					{
						this.props.isAuth && <Menu.Item key="1" icon={<UserOutlined />}>
							<Link to="/users">
								Пользователи
							</Link>
						</Menu.Item>
					}
					{
						this.props.isAuth && <Menu.Item key="2" icon={<VideoCameraOutlined />}>
							<Link to="/categories">
								Категории товаров
							</Link>
						</Menu.Item>

					}
					{
						this.props.isAuth && <Menu.Item key="3" icon={<UploadOutlined />}>
							<Link to="/products">
								Товары
							</Link>
						</Menu.Item>

					}
					{
						this.props.isAuth && <Menu.Item key="4" icon={<UserOutlined />}>
							<Link to="/orders">
								Заказы
							</Link>
						</Menu.Item>

					}
					{
						this.props.isAuth && <Menu.Item key="5" icon={<LogoutOutlined />}>
							<Link to="/logout">
								Выход
							</Link>
						</Menu.Item>
					}
				</Menu>
			</Sider>
		)
	}
}


export default Sidebar;
