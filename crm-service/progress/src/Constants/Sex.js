const sex = {
	male: 'Мужчина',
	female: 'Женщина',
	another: 'Другой',
	unknown: 'Неизвестно',
}

export default sex;
