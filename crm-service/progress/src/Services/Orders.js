import Request from 'axios'
import { notification } from 'antd'
import Config from '../Config'

export const List = async (nav) => {
	const token = localStorage.getItem('app-token');
	const url = `${Config.services.Orders}/orders`

	try {
		const response = await Request.get(url, {
			params: {
				...nav
			},
			headers: {
				'app-token': token,
			}
		});

		return response.data;
	} catch(error) {
		console.log('ERROR', error)
		notification.error({
			message: 'Ошибка при получении списка заказов',
		})
	}

}
