import Request from 'axios'
import { notification } from 'antd'
import Config from '../Config'


export const Create = async (product) => {
	const token = localStorage.getItem('app-token');
	const url = `${Config.services.Products}/products`

	try {
		const response = await Request.post(url, product, {
			headers: {
				'app-token': token,
				'Content-Type': 'multipart/form-data'
			}
		});

		notification.success({
			message: 'Товар добавлен',
			description: 'Теперь можно загрузить фото'
		});

		return response.data;
	} catch(error) {
		console.log('ERROR', error)
		notification.error({
			message: 'Ошибка при создании товара',
		})
	}
}
