import Request from 'axios'
import { notification } from 'antd'
import Config from '../Config'


export const Create = async (category) => {
	const token = localStorage.getItem('app-token');
	const url = `${Config.services.Products}/crm/categories`

	try {
		await Request.post(url, category, {
			headers: {
				'app-token': token,
			},
		});

		notification.success({
			message: 'Категория создана успешно'
		});
	} catch(error){
		console.log('error', error)
		notification.error({
			message: 'Ошибка при создании категории'
		});
	}
}

export const List = async (filters, nav) => {
	const url = `${Config.services.Products}/categories`

	const preparedFilters = {};

	if(filters.name) preparedFilters.name = filters.name
	if(filters.code) preparedFilters.code = filters.code

	try {
		const categories = await Request.get(url, {
			params: {
				...preparedFilters,
				...nav,
			}
		});

		return categories.data
	} catch(error){
		console.log('error', error)
		notification.error({
			message: 'Ошибка при получении списка категорий'
		});
	}
}

export const GetById = async (id) => {
	const url = `${Config.services.Products}/categories/${id}`

	try {
		const response = await Request.get(url);

		return response.data
	} catch(error){
		console.log('error', error)
		notification.error({
			message: 'Ошибка при получении категории'
		});
	}
}

export const Update = async (id, category) => {
	const url = `${Config.services.Products}/crm/categories/${id}`
	const token = localStorage.getItem('app-token');

	try {
		const response = await Request.put(url, category, {
			headers: { 'app-token': token }
		});

		notification.success({
			message: 'Изменение прошло успешно'
		});

		return response.data
	} catch(error){
		console.log('error', error)
		notification.error({
			message: 'Ошибка при изменении категории'
		});
	}
}

