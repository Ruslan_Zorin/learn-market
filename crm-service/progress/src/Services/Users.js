import Request from 'axios'
import { notification } from 'antd'
import Config from '../Config'



export const GetCode = async (phone) => {
	const url = `${Config.services.Users}/code`

	try {
		await Request.get(url, {
			params: { phone }
		});
		notification.success({
			message: 'Код отправлен'
		});
	} catch(error) {
		console.log('ERROR', error);
		notification.error({
			message: 'Ошибка при отправке кода'
		});
	}
}

export const GetToken = async (phone, code) => {
	const url = `${Config.services.Users}/authorization`

	try {
		const response = await Request.post(url, { phone, code });
		localStorage.setItem('app-token', response.data.token)
	} catch(error) {
		console.log('ERROR', error);
		notification.error({
			message: 'Ошибка при авторизации'
		});
	}
}

export const Logout = async () => {
	const url = `${Config.services.Users}/logout`
	const token = localStorage.getItem('app-token');

	await Request.get(url, {
		headers: {
			'app-token': token
		}
	});

	localStorage.clear();
}

export const List = async (filters) => {
	const url = `${Config.services.Users}/crm/users`
	const token = localStorage.getItem('app-token')
	try {
		const response = await Request.get(url, {
			params: filters,
			headers: { 'app-token': token }
		});

		return response.data;
	} catch(error) {
		console.log('ERROR', error);
		notification.error({
			message: 'Ошибка при получении списка пользователей'
		});

		return { total: 0, users: [] };
	}
}

export const AddRole = async (userId) => {
	const url = `${Config.services.Users}/crm/users/${userId}/role`
	const token = localStorage.getItem('app-token')
	try {
		await Request.put(url, {}, { headers: { 'app-token': token } });
	} catch(error) {
		console.log('ERROR', error);
		notification.error({
			message: 'Ошибка при добавлении прав'
		});
	}
}

