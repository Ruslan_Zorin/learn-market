import { combineReducers } from 'redux';

import AppReducer from './App'
import AuthPageReducer from './Pages/Auth'
import UsersPageReducer from './Pages/Users'
import CategoriesPageReducer from './Pages/Categories'
import CategoryPageReducer from './Pages/Category'
import ProductsPageReducer from './Pages/Products'
import OrdersPageReducer from './Pages/Orders'


export default combineReducers({
	app: AppReducer,
	authPage: AuthPageReducer,
	usersPage: UsersPageReducer,
	categoriesPage: CategoriesPageReducer,
	categoryPage: CategoryPageReducer,
	productsPage: ProductsPageReducer,
	ordersPage: OrdersPageReducer,
});
