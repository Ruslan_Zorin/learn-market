const initialState = {
	isAuth: Boolean(localStorage.getItem('app-token')),
	title: ''
}

const AppReducer = (state = initialState, action) => {
	if(action.type === 'UPDATE_AUTH') {
		return {
			...state,
			isAuth: Boolean(localStorage.getItem('app-token'))
		}
	}

	if(action.type === 'LOGOUT') {
		return {
			...state,
			isAuth: false,
		}
	}

	if(action.type === 'SET_TITLE'){
		return {
			...state,
			title: action.data,
		}
	}

	return state;
}

export default AppReducer
