const initialState = {
	users: [],
	total: 0,
	filters: {
		sex: '',
		role: '',
		phone: '',
	},
	nav: {
		lpage: 20,
		page: 1,
	}
}

const UsersPageReducer = (state = initialState, action) => {
	if(action.type === 'SET_USERS'){
		return {
			...state,
			users: action.data.users,
			total: action.data.total,
		}
	}

	if(action.type === 'CHANGE_FILTERS_PHONE'){
		return {
			...state,
			filters: {
				...state.filters,
				phone: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_FILTERS_ROLE'){
		return {
			...state,
			filters: {
				...state.filters,
				role: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_FILTERS_SEX'){
		return {
			...state,
			filters: {
				...state.filters,
				sex: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_USERS_LIMIT_PAGE'){
		return {
			...state,
			nav: {
				...state.nav,
				lpage: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_USERS_PAGE'){
		return {
			...state,
			nav: {
				...state.nav,
				page: action.data,
			}
		}
	}

	return state;
}

export default UsersPageReducer;
