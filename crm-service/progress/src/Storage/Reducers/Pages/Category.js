const initialState = {
	updatedData: {
		name: '',
		description: ''
	},
	name: '',
	code: '',
	description: '',
	isShowModal: false,
};



const CategoryPageReducer = (state = initialState, action) => {
	if(action.type === 'SWITCH_EDIT_CATEGORY_MODAL') {
		return {
			...state,
			isShowModal: !state.isShowModal,
		}
	}

	if(action.type === 'SET_CATEGORY') {
		return {
			...state,
			updatedData: {
				name: action.data.name,
				description: action.data.description,
			},
			...action.data,
		}
	}

	if(action.type === 'CHANGE_CATEGORY_NAME') {
		return {
			...state,
			updatedData: {
				...state.updatedData,
				name: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_CATEGORY_DESCRIPTION') {
		return {
			...state,
			updatedData: {
				...state.updatedData,
				description: action.data,
			}
		}
	}


	return state;
}

export default CategoryPageReducer;
