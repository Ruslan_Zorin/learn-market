const initialState = {
	list: [],
};

const OrdersPageReducer = (state = initialState, action) => {
	if(action.type === 'SET_ORDERS_LIST') {
		return {
			...state,
			list: action.data,
		}
	}

	if(action.type === 'ADD_ORDER_TO_LIST') {
		return {
			...state,
			list: [
				action.data,
				...state.list,
			],
		}
	}
	
	return state;
}

export default OrdersPageReducer;
