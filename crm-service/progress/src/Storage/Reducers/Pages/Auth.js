const initialState = {
	phone: '',
	code: '',
	isHiddenCode: true,
}

const AuthPageReducer = (state = initialState, action) => {
	if(action.type === 'CHANGE_PHONE'){
		return {
			...state,
			phone: action.data,
			isHiddenCode: action.data.length !== 10
		}
	}

	if(action.type === 'CHANGE_CODE'){
		return {
			...state,
			code: action.data,
		}
	}


	return state;
}

export default AuthPageReducer;
