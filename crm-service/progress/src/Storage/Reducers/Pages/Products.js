const initialState = {
	list: [],
	product: {
		id: null,
		name: '',
		vendor_code: '',
		price: 0,
		categories: [],
		description: '',
		files: [],
	},
	isShowModal: false,
	categories: []
};


const ProductsPageReducer = (state = initialState, action) => {
	if(action.type === 'SWITCH_ADD_PRODUCT_MODAL') {
		return {
			...state,
			isShowModal: !state.isShowModal,
		}
	}

	if(action.type === 'ADD_PRODUCTS_FILES') {
		return {
			...state,
			product: {
				...state.product,
				files: [...state.product.files, action.data],
			},
		}
	}

	if(action.type === 'SET_PRODUCTS_CATEGORIES') {
		return {
			...state,
			categories: action.data,
		}
	}

	if(action.type === 'CHANGE_PRODUCT_NAME') {
		return {
			...state,
			product: {
				...state.product,
				name: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_PRODUCT_VENDOR_CODE') {
		return {
			...state,
			product: {
				...state.product,
				vendor_code: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_PRODUCT_PRICE') {
		return {
			...state,
			product: {
				...state.product,
				price: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_PRODUCT_CATEGORIES') {
		return {
			...state,
			product: {
				...state.product,
				categories: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_PRODUCT_DESCRIPTION') {
		return {
			...state,
			product: {
				...state.product,
				description: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_PRODUCT_ID') {
		return {
			...state,
			product: {
				...state.product,
				id: action.data,
			}
		}
	}

	return state;
}

export default ProductsPageReducer;
