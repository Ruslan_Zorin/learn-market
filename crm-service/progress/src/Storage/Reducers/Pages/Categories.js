const initialState = {
	isShowModel: false,
	newCategory: {
		name: '',
		code: '',
		description: '',
	},
	list: [],
	filters: {
		name: '',
		code: '',
	},
	nav: {
		page: 1,
		lpage: 20,
	}
};



const CategoriesPageReducer = (state = initialState, action) => {
	if(action.type === 'SWITCH_CREATE_CATEGORY_MODAL') {
		return {
			...state,
			isShowModel: !state.isShowModel,
		}
	}

	if(action.type === 'CHANGE_NEW_CATEGORY_NAME') {
		return {
			...state,
			newCategory: {
				...state.newCategory,
				name: action.data,
			},
		}
	}

	if(action.type === 'CHANGE_NEW_CATEGORY_CODE') {
		return {
			...state,
			newCategory: {
				...state.newCategory,
				code: action.data,
			},
		}
	}

	if(action.type === 'CHANGE_NEW_CATEGORY_DESCRIPTION') {
		return {
			...state,
			newCategory: {
				...state.newCategory,
				description: action.data,
			},
		}
	}

	if(action.type === 'CLEAR_NEW_CATEGORY') {
		return {
			...state,
			newCategory: initialState.newCategory,
		}
	}
	
	if(action.type === 'SET_CATEGORIES') {
		return {
			...state,
			list: action.data
		}
	}

	if(action.type === 'CHANGE_FILTERS_NAME') {
		return {
			...state,
			filters: {
				...state.filters,
				name: action.data,
			}
		}
	}

	if(action.type === 'CHANGE_FILTERS_CODE') {
		return {
			...state,
			filters: {
				...state.filters,
				code: action.data,
			}
		}
	}
	
	if(action.type === 'CLEAR_CATEGORIES_FILTERS') {
		return {
			...state,
			filters: initialState.filters,
		}
	}

	return state;
}

export default CategoriesPageReducer;
