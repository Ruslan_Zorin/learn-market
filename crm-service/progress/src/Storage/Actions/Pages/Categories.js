export const switchModal = (dispatch) => () => {
	dispatch({ type: 'SWITCH_CREATE_CATEGORY_MODAL' })
}

export const changeCategoryName = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_NEW_CATEGORY_NAME', data })
}

export const changeCategoryCode = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_NEW_CATEGORY_CODE', data })
}

export const changeCategoryDescription = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_NEW_CATEGORY_DESCRIPTION', data })
}

export const clearNewCategory = (dispatch) => () => {
	dispatch({ type: 'CLEAR_NEW_CATEGORY' })
}

export const setCategories = (dispatch) => (data) => {
	dispatch({ type: 'SET_CATEGORIES', data })
}

export const changeFiltersName = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_FILTERS_NAME', data })
}

export const changeFiltersCode = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_FILTERS_CODE', data })
}

export const clearFilters = (dispatch) => () => {
	dispatch({ type: 'CLEAR_CATEGORIES_FILTERS' })
}



