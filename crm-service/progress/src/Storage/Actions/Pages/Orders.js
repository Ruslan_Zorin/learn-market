export const setOrders = (dispatch) => (data) => {
	dispatch({ type: 'SET_ORDERS_LIST', data })
}

export const addOrder = (dispatch) => (data) => {
	dispatch({ type: 'ADD_ORDER_TO_LIST', data })
}
