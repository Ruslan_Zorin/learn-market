export const changePhone = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PHONE', data })
}
export const changeCode = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_CODE', data })
}