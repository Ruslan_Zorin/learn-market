export const setUsers = (dispatch) => (data) => {
	dispatch({ type: 'SET_USERS', data })
}

export const changePhone = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_FILTERS_PHONE', data })
}

export const changeRole = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_FILTERS_ROLE', data })
}

export const changeSex = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_FILTERS_SEX', data })
}

export const changePage = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_USERS_PAGE', data })
}

export const changeLpage = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_USERS_LIMIT_PAGE', data })
}
