export const switchModal = (dispatch) => () => {
	dispatch({ type: 'SWITCH_EDIT_CATEGORY_MODAL' })
}

export const setCategory = (dispatch) => (data) => {
	dispatch({ type: 'SET_CATEGORY', data })
}

export const changeName = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_CATEGORY_NAME', data })
}

export const changeCode = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_CATEGORY_CODE', data })
}

export const changeDescription = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_CATEGORY_DESCRIPTION', data })
}
