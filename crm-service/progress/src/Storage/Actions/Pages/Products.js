export const switchModal = (dispatch) => () => {
	dispatch({ type: 'SWITCH_ADD_PRODUCT_MODAL' })
}

export const changeName = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_NAME', data })
}

export const changeVendorCode = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_VENDOR_CODE', data })
}

export const changePrice = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_PRICE', data })
}

export const changeCategories = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_CATEGORIES', data })
}

export const changeDescription = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_DESCRIPTION', data })
}

export const setCategories = (dispatch) => (data) => {
	dispatch({ type: 'SET_PRODUCTS_CATEGORIES', data })
}

export const changeId = (dispatch) => (data) => {
	dispatch({ type: 'CHANGE_PRODUCT_ID', data })
}

export const addImages = (dispatch) => (data) => {
	dispatch({ type: 'ADD_PRODUCTS_FILES', data })
	
}
