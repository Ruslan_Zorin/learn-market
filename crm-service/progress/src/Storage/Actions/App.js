export const updateAuth = (dispatch) => () => {
	dispatch({ type: 'UPDATE_AUTH' })
}

export const logout = (dispatch) => () => {
	dispatch({ type: 'LOGOUT' })
}

export const setTitle = (dispatch) => (data) => {
	dispatch({ type: 'SET_TITLE', data })
}
