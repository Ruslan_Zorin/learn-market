const { Server } = require('ws');
const WebSockets = require('ws');

const WebSocketsServer = new WebSockets.Server({ port: 9000 })

WebSocketsServer.on('connection', (ws) => {
	ws.on('message', (message) => {
		WebSocketsServer.clients.forEach(client => {
			if(client.readyState === WebSockets.OPEN)
				client.send(message)
		})
	})
	ws.send('My message')
})