class ForbiddenError extends Error {
	constructor(message) {
		super('Forbidden')
		this.status = 403;
		this.data = message
	}
}

module.exports = ForbiddenError;
