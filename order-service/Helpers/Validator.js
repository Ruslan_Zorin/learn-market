const Validator = (schema, data) => {
	const { error } = schema.validate(data, { abortEarly: false })
	if(!error)
		return null;

	return error.details.map(field => {
		return {
			field: field.context.key,
			error: field.type,
		}
	})
}

module.exports = Validator
