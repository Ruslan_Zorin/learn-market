const Koa = require('koa');
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')
const Config = require('./Config')
const Router = require('./Routers')
const Libs = require('./Libs')


const run = async () => {
	const App = new Koa();

	App.listen(Config.SERVER.port, () => console.log(`Server started on port: ${Config.SERVER.port}`))

	App.use(bodyParser())
	App.use(cors())

	Router.init(App)
	Libs.init()
}

run()
