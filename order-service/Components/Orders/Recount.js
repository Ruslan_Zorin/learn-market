module.exports = OrdersSchema => {
	OrdersSchema.methods.recount = function() {
		this.price = 0;

		this.products.forEach((product) => {
			product.total = product.price * product.quantity;

			this.price += product.total;
		});
	};

};
