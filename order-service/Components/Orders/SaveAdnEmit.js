const Broadcast = require('../../Libs/Sockets/Static/Broadcast')


module.exports = OrdersSchema => {
	OrdersSchema.methods.saveAndEmit = async function() {
		await this.save();

		Broadcast('new_order', this);
	};

};
