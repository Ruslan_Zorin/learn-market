const Mongoose = require('../../Libs/Mongoose')


const OrdersSchema = new Mongoose.Schema({
	user: {
		id: String,
		phone: String,
	},
	status: {
		type: String,
		enum: ['created', 'in_progress', 'ready', 'done']
	},
	products: [
		{
			id: String,
			name: String,
			vendor_code: String,
			price: Number, // Цена за штуку
			total: Number, // Цена за позицию
			quantity: Number,
		}
	],
	comments: String,
	price: Number,
});


require('./Recount')(OrdersSchema)
require('./SaveAdnEmit')(OrdersSchema)

module.exports =  Mongoose.model('Orders', OrdersSchema);
