const WebSockets = require('ws');
const Sockets = require('../')

const Broadcast = (type, data) => {
	Sockets.clients.forEach(client => {
		if(client.readyState === WebSockets.OPEN) {
			const message = JSON.stringify({ type, data })

			client.send(message)
		}
	})
}

module.exports = Broadcast
