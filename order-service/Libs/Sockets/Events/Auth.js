const Redis = require('../../Redis')


const auth = async (ws, token) => {
	const userData = await Redis.get(`auth_token_${token}`)
	if(userData) {
		console.log('Auth success')
		return ws.auth = JSON.parse(userData)
	}

	console.log('Auth Error')
	ws.close()
}

module.exports = auth;
