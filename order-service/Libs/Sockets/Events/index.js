const Auth = require('./Auth')

const socketsMethods = async (ws, type, data) => {
	if(type === 'auth') await Auth(ws, data.token)
}

module.exports = socketsMethods;
