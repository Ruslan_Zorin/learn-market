const WebSockets = require('ws');
const Events = require('./Events')

const WebSocketsServer = new WebSockets.Server({ port: 9000 })

WebSocketsServer.on('connection', (ws) => {
	ws.auth = null;

	ws.on('message', async (message) => {
		const { type, data } = JSON.parse(message);
		await Events(ws, type, data);
	})
})

module.exports = WebSocketsServer
