const init = () => {
	require('./Redis')
	require('./Mongoose')
	require('./Sockets')
}

module.exports = { init }
