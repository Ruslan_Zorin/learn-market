const KoaRouter = require('koa-router')
const Router = new KoaRouter();
const ErrorsMiddleware = require('../Middleware/Errors')
const AuthMiddleware = require('../Middleware/Auth')
const IsAdminMiddleware = require('../Middleware/IsAdmin')

const CreateOrdersController = require('../Controllers/Orders/Create')
const GetOrdersController = require('../Controllers/Orders/Get')
const EditOrdersController = require('../Controllers/Orders/Edit')


const init = (App) => {
	App.use(ErrorsMiddleware)
	Router.get('/', (ctx) => ctx.body = 'Service is working')

	Router.post('/orders', AuthMiddleware, CreateOrdersController)
	Router.get('/orders', AuthMiddleware, IsAdminMiddleware, GetOrdersController)
	Router.put('/orders/:id', AuthMiddleware, IsAdminMiddleware, EditOrdersController)

	App.use(Router.routes())
}

module.exports = { init }
