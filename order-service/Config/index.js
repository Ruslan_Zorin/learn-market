const ConfigDefault = require('./default.json')
const Services = require('./services.json')


module.exports = {
	...ConfigDefault,
	url: service => Services[service],
}
