const Joi = require('joi')
const Request = require('superagent')
const Config = require('../../Config')
const Validator = require('../../Helpers/Validator')
const Orders = require('../../Components/Orders')
const BadRequestError = require('../../Errors/BadRequestError')

const Schema = Joi.object({
	products: Joi.array().items(Joi.object({
		id: Joi.string().required(),
		quantity: Joi.number().required()
	})),
	comments: Joi.string().optional(),
});

const CreateProductController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { comments = '', products } = ctx.request.body

	const remoteProducts = await Request.post(`${Config.url('products')}/products/ids`)
		.send({
			id: ctx.request.body.products.map(({ id }) => id)
		})
		.set('Content-Type', 'application/json')

	const order = new Orders.Model({
		products: products.reduce((acc, product) => {
			const currentProduct = remoteProducts.body.products.find(({ _id }) => _id === product.id)
			if(!currentProduct)
				return acc;

			product.price = currentProduct.price;
			product.name = currentProduct.name
			product.vendor_code = currentProduct.vendor_code
			acc.push(product);

			return acc;
		}, []),
		comments,
		user: {
			id: ctx.user.id,
			phone: ctx.user.phone,
		}
	});

	order.recount();
	await order.saveAndEmit();

	ctx.body = { order }
};

module.exports = CreateProductController;
