const Joi = require('joi')
const Validator = require('../../Helpers/Validator')
const BadRequestError = require('../../Errors/BadRequestError')
const Orders = require('../../Components/Orders')

const Schema = Joi.object({
	status: Joi.string().optional(),
	user_id: Joi.string().optional(),
	user_phone: Joi.string().optional(),
	lpage: Joi.number().required(),
	page: Joi.number().required(),
});

const GetController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.query)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const {
		status = '',
		user_id = '',
		user_phone = '',
		lpage = 20,
		page = 1,
	} = ctx.request.query;

	const limit = Number(lpage);

	const filters = {};
	if(status)
		filters.status = status
	if(user_id)
		filters['user.id'] = user_id
	if(status)
		filters['user.phone'] = user_phone

	const orders = await Orders.Model.find(filters)
		.limit(limit)
		.skip((Number(page) - 1) * limit)

	ctx.body = { orders }
}

module.exports = GetController
