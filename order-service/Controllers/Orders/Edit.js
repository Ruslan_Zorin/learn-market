const Joi = require('joi')
const Validator = require('../../Helpers/Validator')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')
const Orders = require('../../Components/Orders')

const Schema = {
	params: Joi.object({
		id: Joi.string().length(24).required()
	}),
	body: Joi.object({
		status: Joi.string().optional(),
		comments: Joi.string().optional(),
	})
};


const EditController = async (ctx) => {
	let Errors = Validator(Schema.params, ctx.request.params)
	ctx.assert(!Errors, new BadRequestError(Errors))

	Errors = Validator(Schema.body, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))


	const { id } = ctx.request.params;

	const order = await Orders.Model.findById(id)
	ctx.assert(order, new NotFoundError('Order not found'))

	order.set(ctx.request.body)
	await order.save();

	ctx.body = { message: 'OK' }
}

module.exports = EditController
