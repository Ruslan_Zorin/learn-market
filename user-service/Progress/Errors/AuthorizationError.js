class AuthorizationError extends Error {
	constructor(message) {
		super('Authorization Error')
		this.status = 401;
		this.data = message;
	}
}

module.exports = AuthorizationError;
