class NotFoundError extends Error {
	constructor(message) {
		super('Not Found')
		this.status = 404;
		this.data = message
	}
}

module.exports = NotFoundError

