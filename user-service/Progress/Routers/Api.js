const AuthMiddleware = require('../Middleware/Auth')
const GetUsersById = require('../Controllers/Users/GetById')


const init = (Router) => {
	Router.get('/users/:id', AuthMiddleware, GetUsersById)
}


module.exports = { init }
