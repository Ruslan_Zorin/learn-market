const GetCodeController = require('../Controllers/GetCode')
const AuthController = require('../Controllers/Authorization')
const LogoutController = require('../Controllers/Logout')
const AuthMiddleware = require('../Middleware/Auth')


const init = (Router) => {
	Router.get('/code', GetCodeController)
	Router.post('/authorization', AuthController)
	Router.get('/logout', AuthMiddleware, LogoutController)
}


module.exports = { init }
