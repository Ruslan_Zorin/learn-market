const KoaRouter = require('koa-router')
const CommonRouter = require('./Common')
const CrmRouter = require('./Crm')
const ApiRouter = require('./Api')
const ErrorsMiddleware = require('../Middleware/Errors')

const Router = new KoaRouter();



const init = (App) => {
	App.use(ErrorsMiddleware)
	Router.get('/', (ctx) => ctx.body = 'Service is working')

	CommonRouter.init(Router)
	CrmRouter.init(Router)
	ApiRouter.init(Router)

	App.use(Router.routes())
}

module.exports = { init }
