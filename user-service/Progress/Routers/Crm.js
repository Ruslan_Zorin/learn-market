const AuthMiddleware = require('../Middleware/Auth')
const IsAdminMiddleware = require('../Middleware/IsAdmin')
const AddRoleUsers = require('../Controllers/Users/AddRoles')
const GetUsers = require('../Controllers/Users/Get')


const init = (Router) => {
	Router.get('/crm/users', AuthMiddleware, IsAdminMiddleware, GetUsers)
	Router.put('/crm/users/:id/role', AuthMiddleware, IsAdminMiddleware, AddRoleUsers)
}


module.exports = { init }
