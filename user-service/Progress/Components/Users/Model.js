const Mongoose = require('../../Libs/Mongoose')


const UsersSchema = new Mongoose.Schema({
	name: String,
	surname: String,
	phone: {
		type: String,
		unique: true
	},
	role: {
		type: String,
		enum: ['user', 'admin'],
		default: 'user',
	},
	sex: String,
	date_registration: {
		type: Date,
		default: Date.now
	}
});

module.exports =  Mongoose.model('Users', UsersSchema);
