const Request = require('superagent');
const Config = require('../Config')


const telegram = (text) => {
	const url = `https://api.telegram.org/bot${Config.TELEGRAM.token}/sendMessage`
	const data = { chat_id: Config.TELEGRAM.chat_id, text, }

	return Request.post(url)
		.send(data)
		.set('Content-Type', 'application/json');
}

const sms = (phone, text) => {
	// TODO
	console.log(`${phone}: ${text}`)
}

const Send = (phone, text) => {
	return Config.ENV === 'prod'
		? sms(phone, text)
		: telegram(text)
}

module.exports = { Send }
