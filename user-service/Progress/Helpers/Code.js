const Config = require('../Config')


const getCode = () => {
	const { length } = Config.CODE

	const code = Array(length).fill(-1)
		.map(() => Math.floor(Math.random() * 9))

	return code.join('')
}

module.exports = { getCode }
