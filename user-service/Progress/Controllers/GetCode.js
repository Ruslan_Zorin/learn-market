const Joi = require('joi')
const SMS = require('../Helpers/SMS')
const Code = require('../Helpers/Code')
const Redis = require('../Libs/Redis')
const Validator = require('../Helpers/Validator')

const Schema = Joi.object({
	phone: Joi.string()
		.pattern(new RegExp('^[0-9]{9}[0-9]$'))
		.required(),
})

const GetCodeController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.query)
	if(Errors) {
		ctx.status = 400;
		return ctx.body = Errors
	}

	const { phone } = ctx.request.query;
	const usersCode = await Redis.get(phone)

	if(usersCode){
		ctx.status = 400;
		return ctx.body = 'Code already sent'
	}

	const SmsCode = Code.getCode();
	await SMS.Send(phone, `Код для входа ${SmsCode}`)

	await Redis.set(phone, SmsCode, ['NX', 'EX'], 120)

	ctx.body = { message: 'OK' }
}

module.exports = GetCodeController
