const Users = require('../../Components/Users')


const AddRolesController = async (ctx) => {
	const { id } = ctx.request.params;

	const user = await Users.Model.findById(id)

	if(!user) {
		ctx.status = 404;
		ctx.body = 'User not found'
		return 
	}

	user.role = user.role === 'admin'
		? 'user'
		: 'admin';

	await user.save();

	ctx.body = { message: 'OK' };
}

module.exports = AddRolesController;
