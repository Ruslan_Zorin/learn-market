const Joi = require('joi');
const Users = require('../../Components/Users');
const Validator = require('../../Helpers/Validator')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')
const ForbiddenError = require('../../Errors/ForbiddenError')


const Schema = Joi.object({
	id: Joi.string()
		.length(24)
		.required()
})

const GetByIdController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.params)

	ctx.assert(!Errors, new BadRequestError(Errors))

	const { id } = ctx.request.params;

	const user = await Users.Model.findById(id)

	ctx.assert(user, new NotFoundError('User not found'))
	ctx.assert(
		ctx.user._id !== String(user._id) && ctx.user.role !== 'admin',
		new ForbiddenError('Permissions denied')
	)

	ctx.body = { user }
}

module.exports = GetByIdController;
