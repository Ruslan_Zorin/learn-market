const Joi = require('joi');
const Users = require('../../Components/Users');
const Validator = require('../../Helpers/Validator')


const Schema = Joi.object({
	phone: Joi.string().optional(),
	name: Joi.string().optional(),
	surname: Joi.string().optional(),
	role: Joi.string().optional(),
	sex: Joi.string().optional(),
	lpage: Joi.number().optional(),
	page: Joi.number().optional(),
});

const GetByIdController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.query)
	if(Errors) {
		ctx.status = 400;
		return ctx.body = Errors
	}

	const {
		phone = '',
		name = '',
		surname = '',
		role = '',
		sex = '',
		lpage = 20,
		page = 1,
	} = ctx.query;
	const limit = Number(lpage)
	const filters = {}

	if(phone) filters.phone = { $regex: `.*${phone}.*` };
	if(name) filters.name = { $regex: `.*${name}.*` };
	if(surname) filters.surname = { $regex: `.*${surname}.*` };
	if(role) filters.role = role
	if(sex && sex !== 'unknown') filters.sex = sex

	const users = await Users.Model.find(filters)
		.skip((page - 1) * limit)
		.limit(limit)
	const total = await Users.Model.countDocuments(filters)

	ctx.body = { users: users.map(user => ({
		id: user._id,
		name: user.name || '',
		surname: user.surname || '',
		role: user.role,
		phone: user.phone,
		birthday: user.birthday || '',
		sex: user.sex || '',
		date_registration: user.date_registration,
	})), total }
}

module.exports = GetByIdController;
