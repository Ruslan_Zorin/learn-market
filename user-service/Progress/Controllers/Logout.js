const Redis = require('../Libs/Redis')


const LogoutController = async (ctx) => {
	const { 'app-token': token } = ctx.request.headers


	await Redis.del(`auth_token_${token}`)
	ctx.body = { message: 'OK' }
}


module.exports = LogoutController
