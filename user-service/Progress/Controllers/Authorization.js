const JWT = require('jsonwebtoken');
const Joi = require('joi');
const Config = require('../Config')
const Redis = require('../Libs/Redis')
const Users = require('../Components/Users');
const Validator = require('../Helpers/Validator')


const Schema = Joi.object({
	phone: Joi.string()
		.pattern(new RegExp('^[0-9]{9}[0-9]$'))
		.required(),
	code: Joi.string()
		.length(6)
		.required(),
})

const AuthController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.body)
	if(Errors) {
		ctx.status = 400;
		return ctx.body = Errors
	}

	const { phone, code } = ctx.request.body;

	const user = await Users.Model.findOne({ phone });
	if(!user)
		await Users.Model.create({ phone });

	const authCode = await Redis.get(phone)

	if(authCode !== code) {
		ctx.status = 401;
		ctx.body = { message: 'Code invalid' }
		return
	}

	const token = JWT.sign({
		phone: user.phone,
		name: user.name || '',
		role: user.role,
		date: new Date().valueOf(),
	}, Config.AUTH.key)

	await Redis.set(`auth_token_${token}`, JSON.stringify(user));
	
	ctx.body = { token };
}

module.exports = AuthController;

