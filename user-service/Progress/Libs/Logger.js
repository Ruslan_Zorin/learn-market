const { transports, createLogger, format } = require('winston')


const ErrorTransports = new transports.File({
	filename: './logs/Error.log',
	level: 'error'
})
const InfoTransports = new transports.File({
	filename: './logs/Info.log',
	level: 'info'
})

const Logger = createLogger({
	format: format.combine(
		format.colorize(),
		format.timestamp(),
		format.align(),
		format.printf(info => {
			const { timestamp = '', level = '', message = '' } = info

			return `[${timestamp}] <${level}>: ${message.trim()}`
		}),

	),
	transports: [
		new transports.Console(),
		ErrorTransports,
		InfoTransports,
	]
})

module.exports = Logger
