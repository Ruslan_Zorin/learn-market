const Mongoose = require('mongoose')
const Config = require('../Config')


Mongoose.connect(
	`mongodb://${Config.DB.host}/${Config.DB.name}`,
	{ useNewUrlParser: true, useUnifiedTopology: true }
).then(console.log('Db Connected'));

module.exports = Mongoose
