const IORedis = require('ioredis');
const Config = require('../Config')

const Redis = new IORedis({
	port: Config.REDIS.port,
	host: Config.REDIS.host,
	password: Config.REDIS.password,
	db: Config.REDIS.db,
});

console.log('Redis connected')

module.exports = Redis
