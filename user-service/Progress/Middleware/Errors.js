const Logger = require('../Libs/Logger')


const ErrorMiddleware = async (ctx, next) => {
	try {
		return await next();
	} catch(error) {
		Logger.error({ message: `${error.message}: ${error.status}: ${JSON.stringify(error.data)}` })

		ctx.status = error.status || 500;
		ctx.body = {
			message: error.message,
			data: error.data
		}
	}
}

module.exports = ErrorMiddleware
