const init = () => {
	require('./Redis')
	require('./Mongoose')
}

module.exports = { init }
