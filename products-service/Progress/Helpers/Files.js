const fs = require('fs')
const util = require('util')
const asyncFsRename = util.promisify(fs.rename);
const Images = require('../Components/Images');



const Upload = async (files, prefix) => {
	const filesName = [];

	await Promise.all(files.map(async (file) => {
		const ext = file.originalname.split('.').pop()

		const newImage = await Images.Model.create({ name: file.originalname })
		const fileName = `${prefix}_${newImage._id}.${ext}`
		filesName.push(fileName)

		return asyncFsRename(file.path, `static/public/${fileName}`)
	}))

	return filesName;
}

module.exports = { Upload }
