const KoaRouter = require('koa-router')
const Router = new KoaRouter();
const ErrorsMiddleware = require('../Middleware/Errors')
const AuthMiddleware = require('../Middleware/Auth')
const IsAdminMiddleware = require('../Middleware/IsAdmin')

const UploadMiddleware = require('../Middleware/Upload')

const CreateCategoryController = require('../Controllers/Categories/Create')
const EditCategoryController = require('../Controllers/Categories/Edit')
const GetCategoriesController = require('../Controllers/Categories/Get')
const GetCategoriesByIdController = require('../Controllers/Categories/GetById')

const CreateProductsController = require('../Controllers/Products/Create')
const AddProductsImagesController = require('../Controllers/Products/AddImages')
const DeleteProductsController = require('../Controllers/Products/Delete')
const GetProductByIdController = require('../Controllers/Products/GetById')
const GetProductsController = require('../Controllers/Products/Get')
const EditProductsController = require('../Controllers/Products/Edit')
const GetProductsByIdsController = require('../Controllers/Products/GetByIds')


const init = (App) => {
	App.use(ErrorsMiddleware)
	Router.get('/', (ctx) => ctx.body = 'Service is working')

	Router.post('/crm/categories', AuthMiddleware, IsAdminMiddleware, CreateCategoryController)
	Router.put('/crm/categories/:id', AuthMiddleware, IsAdminMiddleware, EditCategoryController)
	Router.get('/categories', GetCategoriesController)
	Router.get('/categories/:id', GetCategoriesByIdController)

	Router.post('/products', AuthMiddleware, IsAdminMiddleware, UploadMiddleware.array('images'), CreateProductsController)
	Router.delete('/products/:id', AuthMiddleware, IsAdminMiddleware, DeleteProductsController)
	Router.put('/products/:id', AuthMiddleware, IsAdminMiddleware, EditProductsController)
	Router.get('/products/:id', GetProductByIdController)
	Router.get('/products/:id', GetProductsController)
	Router.post('/products/ids', GetProductsByIdsController)

	App.use(Router.routes())
}

module.exports = { init }
