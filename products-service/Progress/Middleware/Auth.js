const JWT = require('jsonwebtoken');
const Config = require('../Config')
const Redis = require('../Libs/Redis')


const AuthMiddleware = async (ctx, next) => {
	const { 'app-token': token } = ctx.request.headers
	let user = null;

	try {
		user = JWT.verify(token, `${Config.AUTH.key}`)
	} catch(error) {
		ctx.status = 401;
		ctx.body = 'token is invalid'
		return
	}
	const redisData = await Redis.get(`auth_token_${token}`);
	const UsersData = JSON.parse(redisData);

	if(!UsersData || UsersData.phone !== user.phone) {
		ctx.status = 401;
		ctx.body = 'token is invalid'
		return
	}

	ctx.user = UsersData

	await next();
}

module.exports = AuthMiddleware;
