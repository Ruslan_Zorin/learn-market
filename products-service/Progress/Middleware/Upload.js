const Multer  = require('@koa/multer')
const Config = require('../Config')

const upload = Multer({ dest: Config.UPLOAD.tmp_dir })


module.exports = upload;
