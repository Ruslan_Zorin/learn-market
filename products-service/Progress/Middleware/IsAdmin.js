const CheckPermissionsMiddleware = async (ctx, next) => {

	const { role } = ctx.user

	if(role === 'admin')
		return await next();

	ctx.status = 403;
	ctx.body = 'Forbidden'
}

module.exports = CheckPermissionsMiddleware
