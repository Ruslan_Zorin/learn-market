const ErrorMiddleware = async (ctx, next) => {
	try {
		return await next();
	} catch(error) {
		ctx.status = error.status || 500;
		ctx.body = {
			message: error.message,
			data: error.data
		}
	}
}

module.exports = ErrorMiddleware
