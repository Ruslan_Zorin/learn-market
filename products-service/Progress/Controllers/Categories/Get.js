const Joi = require('joi');
const Categories = require('../../Components/Categories')
const BadRequestError = require('../../Errors/BadRequestError')
const Validator = require('../../Helpers/Validator')


const Schema = Joi.object({
	name: Joi.string().optional(),
	code: Joi.string().optional(),
	lpage: Joi.number().optional(),
	page: Joi.number().optional(),
});

const GetController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors));

	const { lpage = 20, page = 1, ...filters } = ctx.request.query;

	const categories = await Categories.Model.find(filters)
		.skip(Number(lpage) * (Number(page) - 1))
		.limit(Number(lpage));

	ctx.body = {
		categories: categories.map(category => category.getCategoriesList())
	}
}

module.exports = GetController
