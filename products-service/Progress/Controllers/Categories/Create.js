const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Categories = require('../../Components/Categories')
const BadRequestError = require('../../Errors/BadRequestError')

const Schema = Joi.object({
	name: Joi.string().required(),
	code: Joi.string().required(),
	description: Joi.string().optional(),
});

const CreateController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { code } = ctx.request.body;

	const category = await Categories.Model.findOne({ code })
	ctx.assert(!category, new BadRequestError([{ field: 'code', error: 'Category already exists' }]))

	await Categories.Model.create(ctx.request.body)

	ctx.body = { message: 'OK' }
}

module.exports = CreateController
