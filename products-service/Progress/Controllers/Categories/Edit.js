const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Categories = require('../../Components/Categories')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')


const Schema = {
	body: Joi.object({
		name: Joi.string().optional(),
		description: Joi.string().optional(),
	}),
	params: Joi.object({
		id: Joi.string().length(24).required()
	}),
};

const EditController = async (ctx) => {
	let Errors = Validator(Schema.body, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))

	Errors = Validator(Schema.params, ctx.request.params)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { id } = ctx.request.params

	const category = await Categories.Model.findById(id)

	ctx.assert(category, new NotFoundError('Category not found'))

	category.set(ctx.request.body)
	await category.save();

	ctx.body = { message: 'OK' }
}

module.exports = EditController
