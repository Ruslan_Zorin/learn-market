const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Categories = require('../../Components/Categories')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')


const Schema = Joi.object({
	id: Joi.string().length(24).required()
});

const GetByIdController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.params)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { id } = ctx.request.params

	const category = await Categories.Model.findById(id)
	ctx.assert(category, new NotFoundError('Category not found'));

	ctx.body = { category: category.getCategoriesList() }
}

module.exports = GetByIdController;
