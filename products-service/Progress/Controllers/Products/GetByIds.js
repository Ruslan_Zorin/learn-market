const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Products = require('../../Components/Products')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')


const Schema = Joi.object({
	id: Joi.array().items(Joi.string().length(24)).required()
});

const GetByIdsController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))
	const { id } = ctx.request.body;

	const products = await Products.Model.find({ _id: { $in: id } })

	ctx.body = { products }
}

module.exports = GetByIdsController