const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Products = require('../../Components/Products')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')


const Schema = {
	params: Joi.object({
		id: Joi.string().length(24).required()
	}),
	body: Joi.object({
		name: Joi.array().items(Joi.string()),
		description: Joi.number(),
		price: Joi.number(),
		categories: Joi.array().items(Joi.string())
	}),
};

const EditController = async (ctx) => {
	let Errors = Validator(Schema.params, ctx.request.params)
	ctx.assert(!Errors, new BadRequestError(Errors))

	Errors = Validator(Schema.body, ctx.request.body)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { id } = ctx.request.params;

	const product = await Products.Model.findById(id)
	ctx.assert(product, new NotFoundError('Product not found'))

	product.set(ctx.request.body)
	await product.save();

	ctx.body = { message: 'OK' }
}

module.exports = EditController
