const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Products = require('../../Components/Products')
const BadRequestError = require('../../Errors/BadRequestError')


const Schema = Joi.object({
	categories: Joi.array().items(Joi.string()),
	lpage: Joi.number(),
	page: Joi.number(),
});

const GetController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.query)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { lpage = 20, page = 1, categories = [] } = ctx.request.query;
	const filters = {}
	const limit = Number(lpage)

	if(categories.length > 0)
		filters.categories = { $in: categories }

	const products = await Products.Model.find(filters)
		.limit(limit)
		.skip((Number(page) - 1) * limit)

	ctx.body = { products }
}

module.exports = GetController
