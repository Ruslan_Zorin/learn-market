const Joi = require('joi');
const Validator = require('../../Helpers/Validator')
const Products = require('../../Components/Products')
const BadRequestError = require('../../Errors/BadRequestError')
const NotFoundError = require('../../Errors/NotFoundError')


const Schema = Joi.object({
	id: Joi.string().length(24).required()
});

const DeleteController = async (ctx) => {
	const Errors = Validator(Schema, ctx.request.params)
	ctx.assert(!Errors, new BadRequestError(Errors))

	const { id } = ctx.request.params;

	const product = await Products.Model.findById(id)
	ctx.assert(product, new NotFoundError('Product not found'))

	await product.remove();

	ctx.body = { message: 'OK' }
}

module.exports = DeleteController
