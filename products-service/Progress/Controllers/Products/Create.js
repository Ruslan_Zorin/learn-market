const Joi = require('joi')
const Validator = require('../../Helpers/Validator')
const Products = require('../../Components/Products')
const Files = require('../../Helpers/Files')
const BadRequestError = require('../../Errors/BadRequestError')

const Schema = Joi.object({
	name: Joi.string().required(),
	description: Joi.string().optional(),
	vendor_code: Joi.string().required(),
	price:  Joi.number().required(),
	categories: Joi.array().items(Joi.string()),
});

const CreateProductController = async (ctx) => {
	console.log('!!!')
	// const Errors = Validator(Schema, ctx.request.body)
	// ctx.assert(!Errors, new BadRequestError(Errors))

	const { vendor_code } = ctx.request.body;

	const product = await Products.Model.findOne({ vendor_code });
	ctx.assert(!product, new BadRequestError([
		{ field: 'vendor_code', error: 'Product already exists' }
	]));

	const { body, files } = ctx.request;
	console.log('body>>', body)
	console.log('files>>', files)
	const images = await Files.Upload(files, 'products');
	const productsData = {
		...body,
		images,
	}

	const createdProduct = await Products.Model.create(productsData)
	

	ctx.body = { product: createdProduct }
};

module.exports = CreateProductController;
