const Mongoose = require('../../Libs/Mongoose')


const CategoriesSchema = new Mongoose.Schema({
	name: String,
	code: {
		type: String,
		unique: true,
	},
	description: String,
});

require('./Getters')(CategoriesSchema)

module.exports =  Mongoose.model('Categories', CategoriesSchema);
