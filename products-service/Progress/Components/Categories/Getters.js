module.exports = CategoriesSchema => {

	CategoriesSchema.methods.getCategoriesList = function() {
		return {
			id: this._id,
			name: this.name,
			code: this.code,
			description: this.description,
		};
	};

};
