const Mongoose = require('../../Libs/Mongoose')


const ProductsSchema = new Mongoose.Schema({
	name: String,
	description: String,
	vendor_code: {
		type: String,
		unique: true,
	},
	price: Number,
	categories: [String],
	images: [String]
});


module.exports =  Mongoose.model('Products', ProductsSchema);
