class BadRequestError extends Error {
	constructor(errors) {
		super('Bad Request')
		this.status = 400;
		this.data = { errors }
	}
}

module.exports = BadRequestError

